#!/bin/bash
set -eu
errorcheck() {
  rv=$?
  if [[ $rv -ne 0 ]]; then
    echo "Test failed. Error code: ${rv}"
  fi
  exit $rv
}
trap "errorcheck" EXIT

echo "Testing ./app.sh"
mkdir -p testdata/output
./app.sh > testdata/output/dog1.txt.output
diff testdata/dog1.txt.golden dog1.txt
echo "Success!"